function distance(first, second){
    if(Array.isArray(first) && Array.isArray(second))
    {
        let unique_array=[]
        for(let i=0;i<first.length;i++)
        {
            if(unique_array.indexOf(first[i])==-1)
            {
                unique_array.push(first[i])
            }

        }


        let unique_array2=[]
        for(let i=0;i<second.length;i++)
        {
            if(unique_array2.indexOf(second[i])==-1)
            {
                unique_array2.push(second[i])
            }

        }

        var difference=unique_array.filter(x=>!unique_array2.includes(x)).concat(unique_array2.filter(x=>!unique_array.includes(x)))
        var k=0;
        for(let i in difference)
            k++;
        return k;
    }
    else
        throw new Error('InvalidType');
}

module.exports.distance = distance